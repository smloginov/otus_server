# OTUS Server


## Architecture

Thread Pool

## Benchmarking

```
$ ab -n 50000 -c 100 -r http://localhost:8080/
This is ApacheBench, Version 2.3 <$Revision: 1706008 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)
Completed 5000 requests
Completed 10000 requests
Completed 15000 requests
Completed 20000 requests
Completed 25000 requests
Completed 30000 requests
Completed 35000 requests
Completed 40000 requests
Completed 45000 requests
Completed 50000 requests
Finished 50000 requests


Server Software:        OTUServer
Server Hostname:        localhost
Server Port:            8080

Document Path:          /
Document Length:        72 bytes

Concurrency Level:      100
Time taken for tests:   6.439 seconds
Complete requests:      50000
Failed requests:        0
Non-2xx responses:      50000
Total transferred:      8700000 bytes
HTML transferred:       3600000 bytes
Requests per second:    7765.36 [#/sec] (mean)
Time per request:       12.878 [ms] (mean)
Time per request:       0.129 [ms] (mean, across all concurrent requests)
Transfer rate:          1319.51 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.1      0       5
Processing:     1   13   0.9     13      21
Waiting:        1   13   0.9     13      20
Total:          6   13   0.9     13      21

Percentage of the requests served within a certain time (ms)
  50%     13
  66%     13
  75%     13
  80%     13
  90%     14
  95%     14
  98%     14
  99%     15
 100%     21 (longest request)
```