import os
import sys
import Queue
import socket
import threading
import time
import logging

from urllib import url2pathname

STATUS_HEADERS = {
    200: 'HTTP/1.1 200 OK\r\n',
    404: 'HTTP/1.1 404 NOT FOUND\r\n',
    405: 'HTTP/1.1 405 METHOD NOT ALLOWED\r\n',
    500: 'HTTP/1.1 500 INTERNAL SERVER ERROR\r\n'
}

CONTENT_TYPES = {
    'html': 'text/html',
    'css':  'text/css',
    'js':   'text/javascript',
    'png':  'image/png',
    'jpg':  'image/jpeg',
    'jpeg': 'image/jpeg',
    'gif':  'image/gif',
    'swf':  'application/x-shockwave-flash'
}

HTML_ERROR_403 = b"<html><body><center><h1>Error 403: FORBIDDEN</h1></center></body></html>"
HTML_ERROR_404 = b"<html><body><center><h1>Error 404: Not Found</h1></center></body></html>"
HTML_ERROR_500 = b"<html><body><center><h1>Error 500: Internal Server Error</h1></center></body></html>"
HTML_ERROR_405 = b"<html><body><center><h1>Error 405: Method is not allowed</h1></center></body></html>"

def setup_logging(logger_path=None):
    logging.basicConfig(level=logging.INFO,
                        filename=logger_path,
                        format='%(asctime)s %(levelname).1s %(message)s',
                        datefmt='%Y.%m.%d %H:%M:%S')


class OTUServer(object):

    def __init__(self, host='localhost', port=80, www_dir='/'):
        self.host = host
        self.port = port
        self.www_dir = www_dir
        self.server = None
        self.queue = Queue.Queue()

    def _generate_headers(self, code, ext=None, content_len=None):
        header = ''
        header += STATUS_HEADERS.get(code, '')
        time_now = time.strftime("%a, %d %b %Y %H:%M:%S", time.localtime())
        header += 'Date: {now}\r\n'.format(now=time_now)
        header += 'Server: OTUServer\r\n'
        header += 'Connection: keep-alive\r\n'
        if code == 200:
            header += 'Content-Type: {type}\r\n'.format(
                type=CONTENT_TYPES.get(ext, ''))
            header += 'Content-Length: {len}\r\n'.format(
                len=content_len)
        header += '\r\n'
        return header

    def _process_request(self, request):
        response = ''
        request_string = request.decode()
        request_method = request_string.split(' ')[0]
        if request_method in ('GET', 'HEAD'):
            requested_fn = request_string.split(' ')[1]
            requested_fn = requested_fn.split('?')[0]
            requested_fn = url2pathname(requested_fn)
            if requested_fn == '/':
                requested_fn = '/index.html'

            path_to_file =self.www_dir + requested_fn
            path_to_file = path_to_file.replace('../', '')
            if os.path.isdir(path_to_file):
                path_to_file = os.path.join(path_to_file, 'index.html')
            code = 200

            if os.path.isfile(path_to_file):
                try:
                    with open(path_to_file, 'rb') as f:
                        ext = f.name.split('.')[-1]
                        response = self._generate_headers(
                            200, ext=ext, content_len=os.path.getsize(path_to_file)).encode()
                        if request_method == 'GET':
                            response += f.read()

                except IOError:
                    response = self._generate_headers(500).encode()
                    response += HTML_ERROR_500
                    code = 500

            else:
                response = self._generate_headers(404).encode()
                if request_method == 'GET':
                    response += HTML_ERROR_404
                    code = 404

        else:
            response = self._generate_headers(405).encode()
            response += HTML_ERROR_405
            requested_fn = ''
            code = 405

        logging.info('{request_method} {requested_fn} {code}'.format(
                                                            request_method=request_method,
                                                            requested_fn=requested_fn,
                                                            code=code))

        return response

    def _request_handler(self, queue):
        while True:
            client = queue.get()
            request = client.recv(1024)
            if not request:
                break
            response = self._process_request(request)
            client.send(response)
            client.close()

    def _managing_connections(self):
        while True:
            (client, addr) = self.server.accept()
            self.queue.put(client)

    def create_server(self, workers):
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        try:
            logging.info('bind server on {host}:{port}'. \
                            format(host=self.host,
                                   port=self.port))

            self.server.bind((self.host, self.port))
        except socket.error:
            logging.exception(
                'failed binding server on {host}:{port}'. \
                    format(host=self.host,
                           port=self.port))
            
            sys.exit(1)

        self.server.listen(1024)

        for _ in range(workers):
            thread = threading.Thread(target=self._request_handler,
                                      args=(self.queue,))
            thread.daemon = True
            thread.start()

        self._managing_connections()


def parse_args():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--workers',
                       type=int,
                       default=4,
                       help='number of workers')

    parser.add_argument('--host',
                        type=str,
                        default='localhost',
                        help='host')

    parser.add_argument('--port',
                        type=int,
                        default=8080,
                        help='port')


    parser.add_argument('--dir',
                        type=str,
                        default=os.getcwd(),
                        help='directory')


    return parser.parse_args()



if __name__ == '__main__':

    setup_logging()
    parser = parse_args()

    s = OTUServer(host=parser.host,
                  port=parser.port,
                  www_dir=parser.dir)
    
    s.create_server(workers=parser.workers)
